package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import model.StationModel;

public class AddStationTest {
	private StationController stationController;
	
	@BeforeEach
	void setUp() throws Exception {
		stationController = new StationController();
	}
	
	@ParameterizedTest
	@CsvSource({ "hanoi, hanoi, 1, 1, 1,1, true", "hanoi, hanoi, 1, 1, 1,1, true" })
	void testAddStation(String name, String address, String NumBike, String NumEBike, String NumTBike, String NumEmpty, boolean expected) {
		StationModel station = new StationModel();
		station.setName(name);
		station.setAddress(address);
		station.setNum_of_bikes(Integer.parseInt(NumBike));
		station.setNum_of_ebikes(Integer.parseInt(NumEBike));
		station.setNum_of_twinbikes(Integer.parseInt(NumTBike));
		station.setNum_of_empty_docks(Integer.parseInt(NumEmpty));
		int res = stationController.saveStation(station);
		boolean check = res >= 0 ? true : false; 
		System.out.println(res);
		assertEquals(expected, check);
	}
}
