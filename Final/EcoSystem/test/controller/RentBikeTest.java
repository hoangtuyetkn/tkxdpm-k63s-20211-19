package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import model.PaymentModel;
import model.StationModel;

class RentBikeTest {
	private PaymentController paymentController;
	private BikeController bikeController;
	private StationController stationController;

	@BeforeEach
	void setUp() throws Exception {
		paymentController = new PaymentController();
		bikeController = new BikeController();
		stationController = new StationController();
	}

	@ParameterizedTest
	@CsvSource({ "1, 1, card1, 1, 100, true" })
	void testSavePayment(String idBike, String id_station_rent, String deposit_card_number, String status,
			String deposit_price, boolean expected) {
		PaymentModel newPayment = new PaymentModel();
		newPayment.setId_bike(Integer.parseInt(idBike));
		newPayment.setId_station_rent(Integer.parseInt(id_station_rent));
		newPayment.setDeposit_card_number(deposit_card_number);
		newPayment.setStatus(Integer.parseInt(status));
		newPayment.setDeposit_price(Integer.parseInt(deposit_price));
		int res = paymentController.savePayment(newPayment);
		boolean check = res >= 0 ? true : false;
		System.out.println(res);
		assertEquals(expected, check);
	}

}
