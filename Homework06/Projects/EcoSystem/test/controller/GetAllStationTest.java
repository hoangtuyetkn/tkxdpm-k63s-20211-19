package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.StationModel;

public class GetAllStationTest {
	private StationController stationController;
	
	@BeforeEach
	void setUp() throws Exception {
		stationController = new StationController();
	}

	@Test
	public void testGetAll() {
		List<StationModel> stations = stationController.findAll();
		boolean check = stations.size() >= 0 ? true : false; 
		assertEquals(true, check);
	}
} 
