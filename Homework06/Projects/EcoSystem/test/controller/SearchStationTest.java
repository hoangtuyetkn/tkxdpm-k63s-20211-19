package controller;

import org.junit.Test.None;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import common.exception.PriceRangeException;
import configs.Constants;

public class SearchStationTest {

	private HomeController homeController;
	
	private final String messageZero = Constants.PRICE_GREATER_ZERO;
	private final String messageCompare = Constants.MAX_NOT_LESS_THAN_MIN;
	
	@BeforeEach
	void setUp() throws Exception {
		homeController = new HomeController();
	}
	
	@ParameterizedTest
	@CsvSource({ 
		"-10, 10, " + messageZero, 
		"0, -100, " + messageZero, 
		"-1, -2, " + messageZero, 
		"'', -1, " + messageZero, 
		"-1, '', " + messageZero, 
		"100, 10, " + messageCompare
	})
	public void testValidateRangePriceThrowsException(String minPrice, String maxPrice, String message) {
		Exception thrown = Assertions.assertThrows(PriceRangeException.class, () -> {
	        homeController.validateRangePrice(minPrice, maxPrice);
	    });
		
		Assertions.assertEquals(message, thrown.getMessage());
	}
	
	@ParameterizedTest
	@CsvSource({ 
		"NaN, ''",
		"NaN, NaN"
	})
	public void testValidateNumberFormatThrowsException(String minPrice, String maxPrice) {
		Assertions.assertThrows(NumberFormatException.class, () -> {
	        homeController.validateRangePrice(minPrice, maxPrice);
	    });
	}
	
	@ParameterizedTest
	@CsvSource({ 
		" '', ''",
		"10, ''",
		"10, 100"
	})
	public void testValidateValid(String minPrice, String maxPrice) {
		Assertions.assertDoesNotThrow(() -> {
	        homeController.validateRangePrice(minPrice, maxPrice);
	    });
	}
}
