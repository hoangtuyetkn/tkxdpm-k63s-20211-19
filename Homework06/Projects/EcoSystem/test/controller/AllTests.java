package controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	GetAllStationTest.class, 
	AddStationTest.class , 
	RentBikeTest.class,
	SearchStationTest.class
})
public class AllTests {
// this class remains completely
// empty, being used only as a
// holder for the above
// annotations
	// asList(-1, 2, 3)
}
