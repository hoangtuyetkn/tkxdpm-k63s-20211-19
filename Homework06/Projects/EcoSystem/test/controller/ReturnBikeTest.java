package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import model.PaymentModel;
import model.StationModel;

public class ReturnBikeTest {
	private StationController stationController;
	private PaymentController paymentController;
	
	@BeforeEach
	void setUp() throws Exception {
		paymentController = new PaymentController();
		stationController = new StationController();
	}
	
	@ParameterizedTest
	@CsvSource({ "1, HUST, Hanoi, 100, 1, 100, 100, true",
		"1, HUST, NamDinh, 100, 100, 100, 100, false" })
	void testUpdateStation(String idStation, String nameStation, String address, Integer num_of_bikes,
			Integer num_of_ebikes, Integer num_of_twinbikes, Integer num_of_empty_docks, boolean expected) {
		StationModel stationModel = new StationModel();
		stationModel.setId(Integer.parseInt(idStation));
		stationModel.setName(nameStation);
		stationModel.setAddress(address);
		stationModel.setNum_of_bikes(num_of_bikes);
		stationModel.setNum_of_ebikes(num_of_ebikes);
		stationModel.setNum_of_twinbikes(num_of_twinbikes);
		stationModel.setNum_of_empty_docks(num_of_empty_docks);
		
		stationController.updateStation(stationModel);
		boolean check = true;
		assertEquals(expected, check);
	}
}
