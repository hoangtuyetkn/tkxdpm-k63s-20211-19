package views.screen;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import common.SingletonObject;
import controller.BikeController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.BikeModel;
import utils.Configs;

public class AddBikeScreenHandler extends BaseScreenHandler implements Initializable{
	
	@FXML
	TextField name;

	@FXML
	TextField type;

	@FXML
	TextField weight;

	@FXML
	TextField licensePlate;

	@FXML
	TextField manufacturingDate;

	@FXML
	TextField cost;

	@FXML
	TextField producer;

	@FXML
	TextField battery;

	@FXML
	TextField loadCycles;

	@FXML
	TextField timeRemaining;
	
	@FXML
	Button btnBack;
	
	@FXML
	Button btnSave;
	
	private Integer stationID;

	public AddBikeScreenHandler(Stage stage, String screenPath, Integer stationID) throws IOException {
		super(stage, screenPath);
		this.stationID = stationID;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		btnSave.setOnMouseClicked(ev -> {
			BikeModel bike = new BikeModel();
			bike.setName(name.getText());
			bike.setType(type.getText());
			bike.setLicense_plate(licensePlate.getText());
			bike.setProducer(producer.getText());
			bike.setId_station(stationID);
			bike.setStatus(0);
			try {
				bike.setWeight(Integer.parseInt(weight.getText()));
				bike.setManuafacturing_date(new SimpleDateFormat("dd/MM/yyyy").parse(manufacturingDate.getText()));
				bike.setCost(Integer.parseInt(cost.getText()));
				bike.setBattery(Integer.parseInt(battery.getText()));
				bike.setLoad_cycles(Integer.parseInt(loadCycles.getText()));
				bike.setTime_remaining(Integer.parseInt(timeRemaining.getText()));
				BikeController bikeController = new BikeController();
				int res = bikeController.saveBike(bike);
				if(res > 0) {
					BaseScreenHandler resultScreen = new ResultScreenHandler(this.stage, Configs.RESULT_SCREEN_PATH,
							"ADD BIKE RESULT", "ADD SUCCESSFULLY", "ADD BIKE");
					resultScreen.setPreviousScreen(this);
					resultScreen.setScreenTitle("Result Screen");
					resultScreen.show();
				}
			} catch (NumberFormatException | ParseException e) {
				JOptionPane.showMessageDialog(null, "Invalid information!");
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error!");
			}
		});
		
		btnBack.setOnMouseClicked(e -> {
			SingletonObject instance = SingletonObject.getInstance();
			BaseScreenHandler previousScreen = instance.getPreviousScreen();
			previousScreen.show();
		});
	}

}
