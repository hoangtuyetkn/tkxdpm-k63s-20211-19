package views.screen.home;

public class SearchQueryInfomationRaw {
	private String nameAddressSearch;
	private String minPrice;
	private String maxPrice;
	private String typeBike;
	
	public SearchQueryInfomationRaw(String nameAddressSearch, String minPrice, String maxPrice, String typeBike) {
		super();
		this.nameAddressSearch = nameAddressSearch;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.typeBike = typeBike;
	}

	public String getNameAddressSearch() {
		return nameAddressSearch;
	}

	public void setNameAddressSearch(String nameAddressSearch) {
		this.nameAddressSearch = nameAddressSearch;
	}

	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getTypeBike() {
		return typeBike;
	}

	public void setTypeBike(String typeBike) {
		this.typeBike = typeBike;
	}
}
