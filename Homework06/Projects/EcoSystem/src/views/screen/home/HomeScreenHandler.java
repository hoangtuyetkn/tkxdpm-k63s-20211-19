package views.screen.home;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import common.SearchQueryInfomation;
import common.SingletonObject;
import common.exception.PriceRangeException;
import configs.Path;
import controller.HomeController;
import controller.StationController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.StationModel;
import utils.Configs;
import views.screen.AddStationScreenHandler;
import views.screen.BaseScreenHandler;
import views.screen.ReturnBikeScreenHandler;

public class HomeScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
	private TextField searchQuery;
	
	@FXML
	private TextField minPrice;
	
	@FXML
	private TextField maxPrice;
	
	@FXML
	private ComboBox<String> typeBike;
	
	@FXML
	private ListView<StationModel> listStation; 
	
	@FXML
	private Button searchButton;

	private StationController stationController;
	
	private HomeController homeController;
	
	public HomeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	private BaseScreenHandler thisScreen = this;
	private List<StationModel> stations = null;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// init controller
		stationController = new StationController();
		homeController = new HomeController();
		
		//setup listview
		listStation.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);		
		listStation.setCellFactory(param -> new ListCell<StationModel>() {
            {
                setFont(new Font(15));
            }
            @Override
            protected void updateItem(StationModel item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    setText(item.getShortInfomation());
                    this.setOnMouseClicked(new HomeMouseClickHandler(thisScreen, item, stage));
                } else {
                    setText(null);
                }
            }
        });
		
		// setup button
		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				searchStation();				
			}
		});
		
		String[] _typeBikes = {"all", "standard", "electric", "twin"};
		typeBike.getItems().addAll(_typeBikes);
		typeBike.getSelectionModel().select(0);
		

		// display stations
		stations = stationController.findAll();
		if(stations!= null) displayStation();
		
	}
	
	private void searchStation() {
		// input info
		SearchQueryInfomationRaw info = inputInfo();
		
		try {
			homeController.validateRangePrice(info.getMinPrice(), info.getMaxPrice());
			Integer minPriceInt = info.getMinPrice().equals("") ? 0 : Integer.parseInt(info.getMinPrice());
			Integer maxPriceInt = info.getMaxPrice().equals("") ? 0 : Integer.parseInt(info.getMaxPrice());
			
			SearchQueryInfomation seacrhQuery = new SearchQueryInfomation(
					info.getNameAddressSearch(), 
					minPriceInt, 
					maxPriceInt, 
					info.getTypeBike()
				);
			
			stations = stationController.findByQuery(seacrhQuery);
			if(stations!= null && !stations.isEmpty()) {
				displayStation();
			}else {
				JOptionPane.showMessageDialog(null, "Station not found!!!");
			}
			
			SingletonObject.getInstance().setSearchQueryInfomation(seacrhQuery);
		}catch(NumberFormatException ex){
			JOptionPane.showMessageDialog(null, "Min price or Max price invalid");
		}catch(PriceRangeException ex){
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error!");
		}
	}
	
	private void displayStation() {
		listStation.getItems().clear();
		listStation.getItems().addAll(stations);
	}
	
	private SearchQueryInfomationRaw inputInfo() {
		String _nameAddressQuery = (String)searchQuery.getText();
		String _minPriceStr = (String)minPrice.getText();
		String _maxPriceStr = (String)maxPrice.getText();
		String _typeBike = typeBike.getValue();
		return new SearchQueryInfomationRaw(_nameAddressQuery, _minPriceStr, _maxPriceStr, _typeBike);
	}
	
	/**
	 * redirect to screen previous
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	public void redirectReturnBikeScreen(MouseEvent event) throws IOException {
		ReturnBikeScreenHandler returnBikeScreenHandler = new ReturnBikeScreenHandler(stage, Configs.RETURN_BIKE, thisScreen);
		returnBikeScreenHandler.setScreenTitle("Return Bike Screen");
		SingletonObject.getInstance().addScreen(thisScreen);
		returnBikeScreenHandler.show();
	}
	
	@FXML
	public void redirectAddStationScreen(MouseEvent event) throws IOException {
		AddStationScreenHandler addStationScreenHandler = new AddStationScreenHandler(stage, Configs.ADD_STATION, thisScreen);
		addStationScreenHandler.setScreenTitle("Add Station Screen");
		SingletonObject.getInstance().addScreen(thisScreen);
		addStationScreenHandler.show();
	}
}
