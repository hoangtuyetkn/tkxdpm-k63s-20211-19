package controller;

import common.exception.PriceRangeException;
import configs.Constants;

public class HomeController {
	
	public void validateRangePrice(String minPriceStr, String maxPriceStr) 
			throws NumberFormatException, PriceRangeException{
		Integer minPriceInt = minPriceStr.equals("") ? 0 : Integer.parseInt(minPriceStr);
		Integer maxPriceInt = maxPriceStr.equals("") ? 0 : Integer.parseInt(maxPriceStr);
		
		if(minPriceInt < 0 || maxPriceInt < 0) throw new PriceRangeException(Constants.PRICE_GREATER_ZERO);
				
		if(maxPriceStr.equals("")) return;
			
		if(maxPriceInt < minPriceInt) throw new PriceRangeException(Constants.MAX_NOT_LESS_THAN_MIN);
	}
}
