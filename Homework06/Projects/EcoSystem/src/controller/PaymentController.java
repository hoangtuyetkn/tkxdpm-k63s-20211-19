package controller;

import dao.IPaymentDAO;
import dao.impl.PaymentDAO;
import model.PaymentModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import common.exception.InvalidCardException;
import common.exception.PaymentException;
import common.exception.UnrecognizedException;
import model.CreditCard;
import model.PaymentTransaction;
import subsystem.InterbankInterface;
import subsystem.InterbankSubsystem;
import utils.TypePayment;

public class PaymentController {
	private IPaymentDAO paymentDAO;
	
    private final Integer defaultPrice = 10000;
	
	private final Integer defaultPricePerFifteenMinute = 3000;

	public PaymentController() {
		this.paymentDAO = new PaymentDAO();
	}

	public int savePayment(PaymentModel newPayment) {
		return this.paymentDAO.insert(newPayment);
	}
	
	/**
	 * update payment after refund
	 * 
	 * @param payment
	 * @return
	 */
	public void updatePaymentAfterRefund(PaymentModel payment) {
		this.paymentDAO.update(payment);
	}
	/**
	 * Represent the card used for payment
	 */
	private CreditCard card;

	/**
	 * Represent the Interbank subsystem
	 */
	private InterbankInterface interbank;

	/**
	 * Validate the input date which should be in the format "mm/yy", and then
	 * return a {@link java.lang.String String} representing the date in the
	 * required format "mmyy" .
	 * 
	 * @param date - the {@link java.lang.String String} represents the input date
	 * @return {@link java.lang.String String} - date representation of the required
	 *         format
	 * @throws InvalidCardException - if the string does not represent a valid date
	 *                              in the expected format
	 */
	private String getExpirationDate(String date) throws InvalidCardException {
		String[] strs = date.split("/");
		if (strs.length != 2) {
			throw new InvalidCardException();
		}

		String expirationDate = null;
		int month = -1;
		int year = -1;

		try {
			month = Integer.parseInt(strs[0]);
			year = Integer.parseInt(strs[1]);
			if (month < 1 || month > 12 || year < Calendar.getInstance().get(Calendar.YEAR) % 100 || year > 100) {
				throw new InvalidCardException();
			}
			expirationDate = strs[0] + strs[1];

		} catch (Exception ex) {
			throw new InvalidCardException();
		}

		return expirationDate;
	}

	/**
	 * Pay order, and then return the result with a message.
	 * 
	 * @param amount         - the amount to pay
	 * @param contents       - the transaction contents
	 * @param cardNumber     - the card number
	 * @param cardHolderName - the card holder name
	 * @param expirationDate - the expiration date in the format "mm/yy"
	 * @param securityCode   - the cvv/cvc code of the credit card
	 * @return {@link java.util.Map Map} represent the payment result with a
	 *         message.
	 */
	public Map<String, String> payOrder(int amount, String contents, String cardNumber, String cardHolderName,
			String expirationDate, String securityCode, TypePayment typePayment) {
		Map<String, String> result = new Hashtable<String, String>();
		result.put("RESULT", "PAYMENT FAILED!");
		try {
			this.card = new CreditCard(cardNumber, cardHolderName, Integer.parseInt(securityCode),
					getExpirationDate(expirationDate));

			this.interbank = new InterbankSubsystem();
			PaymentTransaction transaction = null;
			if (Objects.equals(typePayment, TypePayment.PAY)) {
				transaction = interbank.payOrder(card, amount, contents);
			}
			
			if (Objects.equals(typePayment, TypePayment.REFUND)) {
				transaction = interbank.refund(card, amount, contents);
			}

			result.put("RESULT", "PAYMENT SUCCESSFUL!");
			result.put("MESSAGE", "You have succesffully paid the order!");
		} catch (PaymentException | UnrecognizedException ex) {
			result.put("MESSAGE", ex.getMessage());
		}
		return result;
	}
	
	/**
	 * get all list payment need return bike
	 * 
	 * @return
	 */
	public List<PaymentModel> getAllPaymentNeedReturn() {
		return paymentDAO.findAll();
	}
	
	/**
	 * caculate total renting time
	 * 
	 * @param rentingTime
	 * @return
	 */
	public Integer calTotalRetingTime(Instant rentingTime) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Duration duration = Duration.between(rentingTime, Instant.now());
		Integer totalMinuteRentBike = Integer.valueOf((int) duration.toMinutes());
		return totalMinuteRentBike;
	}
	
	/**
	 *  caculate total money which user must pay
	 * 
	 * @param totalTimeMinute
	 * @param typeBike
	 * @return
	 */
	public Integer calPriceRentingBike(Integer totalTimeMinute, String typeBike) {
		Integer priceRent = 0;
		
		// if rentingTime < 0 => free
		if (0 < totalTimeMinute && totalTimeMinute <= 10) {
			priceRent = 0;
		} else if (10 <= totalTimeMinute && totalTimeMinute <= 30) {
			priceRent = defaultPrice;
		} else {
			priceRent = defaultPrice + ((totalTimeMinute - 30) / 15) * defaultPricePerFifteenMinute;
		}
		return priceRent;
	}
	
	/**
	 * caculate total refund money
	 * 
	 * @param payment
	 * @return
	 */
	public Integer calRefundOrder(PaymentModel payment) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Instant rentBikeTime = payment.getRent_time().toInstant();
		Duration duration = Duration.between(rentBikeTime, Instant.now());
		Integer totalMinuteRentBike = Integer.valueOf((int) duration.toMinutes());
		Integer priceRent = 0;
		
		// if rentingTime < 0 => free
		if (0 < totalMinuteRentBike && totalMinuteRentBike <= 10) {
			priceRent = 0;
		} else if (10 <= totalMinuteRentBike && totalMinuteRentBike <= 30) {
			priceRent = defaultPrice;
		} else {
			priceRent = defaultPrice + ((totalMinuteRentBike - 30) / 15) * defaultPricePerFifteenMinute;
		}
		
		Integer amountRefund = payment.getDeposit_price() - priceRent;
		if (amountRefund <= 0) {
			return 0;
		}
		return payment.getBikeModel().getType().equals("standard") ? amountRefund : Integer.valueOf((int) (amountRefund * 1.5));
	}

}
