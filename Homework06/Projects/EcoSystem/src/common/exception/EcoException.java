package common.exception;;

/**
 * The AimsException wraps all unchecked exceptions You can use this
 * exception to inform
 * 
 * @author nguyenlm
 */
public class EcoException extends RuntimeException {

    public EcoException() {

	}

	public EcoException(String message) {
		super(message);
	}
}